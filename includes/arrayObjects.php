<?php   
  
  $serviceCol=array(
        array(
            "programs.php#science",
            "service-science",
            "icon-atom",
            "Science",
            "The application of scientific knowledge by inspired learners leads to a brighter future for the environment and the organisms around us."
        ),

         array(
            "programs.php#technology",
            "service-technology",
            "icon-computer",
            "Technology",
            "Understanding the development of new technologies, students can become the innovative masterminds behind their own developments.
"
        ),    

         array(
            "programs.php#engineering",
            "service-engineering",
            "icon-engineer-1",
            "Engineering",
            "As everything around us is advancing due to the complexity of engineering, employing challenges to students will inspire them to be creators of such innovations.
"
        ),   

        array(
            "programs.php#maths",
            "service-maths",
            "icon-calculating",
            "Mathematics",
            "Math holds the power of reasoning and problem-solving, a skill that is needed everywhere and everyday for effective attainment of our goals."
        ),
        
    );

    //blog items
     $blogItem=array(
        array(
            "blog01",
            " Impact of STEM Experience on Future Endeavors",
            "February 05, 2021",
            "impactOfStemBlogDetail"
        ),
        array(
            "blog02",
            "STEM in Real Life",
            "February 06, 2021",
            "stemRealLifeBlogDetail"
        ),       
    );

?>