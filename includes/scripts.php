 <!-- scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/swiper-bundle.min.js"></script>
    <script src="js/easyResponsiveTabs.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/bsnav.min.js"></script>
    <!--[if lte IE 9]>
            <script src="js/ie.lteIE9.js"></script>
        <![endif]-->    
    <script>
        //animation
        wow = new WOW(
            {
                animateClass: 'animate__animated',
                offset: 100,
                mobile: true
            }
        );
        wow.init(); 
    </script>
    <script src="js/custom.js"></script>

    <script>
        //contact form validatin
        $('#contact_form').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },

            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10
                },
                email: {
                    required: true,
                    email: true
                },
                howDid: {
                    required: true
                },
                sub: {
                    required: true,
                }
            },

            messages: {
                name: {
                    required: "Enter Name"
                },
                phone: {
                    required: "Enter Valid Mobile Number"
                },
                email: {
                    required: "Enter Valid Email"
                },
                email: {
                    required: "How did you know abut us"
                },
                sub: {
                    required: "Enter Subject"
                }
            },
        });
    </script>
    <!--/ scripts -->