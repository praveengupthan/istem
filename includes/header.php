<!-- header -->
    <header>        
            <div class="navbar navbar-expand-lg bsnav bsnav-sticky bsnav-sticky-slide">
                <a class="navbar-brand" href="index.php">
                    <img src="img/logo.svg" alt="iSTEMi">
                </a>
                <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse justify-content-md-between">
                    <ul class="navbar-nav navbar-mobile mx-sm-auto midNav">
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" href="index.php"><span>Home</span></a></li>  
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';}?>" href="about.php"><span>About us</span></a></li>    
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='founders.php'){echo'active';}else {echo'nav-link';}?>" href="founders.php"><span>Founders</span></a></li>                                      
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='programs.php'){echo'active';}else {echo'nav-link';}?>" href="programs.php"><span>Programs</span></a></li>   
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='summerProgram2021.php'){echo'active';}else {echo'nav-link';}?>" href="summerProgram2021.php"><span>Summer Program 2021</span></a></li>
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='curriculum.php'){echo'active';}else {echo'nav-link';}?>" href="curriculum.php"><span>Curriculum</span></a></li>                     
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='blog.php'){echo'active';}else {echo'nav-link';}?>" href="blog.php">Blog</a></li>                       
                        <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';}?>" href="contact.php"><span>Contact</span></a></li>                        
                    </ul>
                    <ul class="navbar-nav navbar-mobile mr-0 socialNav">
                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"><span class="icon-facebook icomoon"></span></a></li>
                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"><span class="icon-twitter icomoon"></span></a></li>
                        <li class="nav-item"><a class="nav-link" href="javascript:void(0)"><span class="icon-linkedin icomoon"></span></a></li>
                    </ul>
                </div>               
            </div>           
            
            <div class="bsnav-mobile">
                <div class="bsnav-mobile-overlay"></div>
                <div class="navbar"></div>
            </div>       
    </header>
    <!--/ header -->