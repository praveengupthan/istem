<!--  styles -->
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">    
    <link rel="stylesheet" href="css/swiper-bundle.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/easy-responsive-tabs.css">
    <link rel="stylesheet" href="css/bsnav.min.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <!--/ styles -->