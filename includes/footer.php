 <!-- footer -->
    <footer>
        <a href="javascript:void(0)" class="moveTop" id="moveTop"><span class="icon-chevron-up icomoon"></span></a>
        <!-- container -->
        <div class="container">
            <ul class="nav justify-content-center">
                <li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="about.php" class="nav-link">About</a></li>
                 <li class="nav-item"><a href="founders.php" class="nav-link">Founders</a></li>
                <li class="nav-item"><a href="programs.php" class="nav-link">Programs</a></li>
                <li class="nav-item"><a href="curriculum.php" class="nav-link">Curriculum</a></li>
                <!-- <li class="nav-item"><a href="blog.php" class="nav-link">Blog</a></li>                -->
                <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>               
            </ul>
           <div class="footer-social text-center">
               <a href="javascript:void(0)" target="_blank"><span class="icon-facebook icomoon"></span></a>
               <a href="javascript:void(0)" target="_blank"><span class="icon-twitter icomoon"></span></a>
               <a href="javascript:void(0)" target="_blank"><span class="icon-linkedin icomoon"></span></a>
           </div>
           <p class="text-center fwhite">
               <i><small>Copyright © 2021 iSTEMi. All rights reserved.</small></i>
           </p>
        </div>
        <!--/ container -->
    </footer>
<!--/ footer -->