<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>

    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
    ?>
</head>
<body>

    <?php include 'includes/header.php' ?>

    <!-- main -->

    <!-- slider -->
    <div class="homeSlider">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-7 sliderImg">
                    <img src="img/sliderImg.svg" alt="" class="img-fluid">
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-md-5 align-self-center">
                    <article class="sliderArticle">
                        <h6>Come and join us</h6>
                        <h1>Creating a community of lifelong learners, explorers of a visionary future, and advocates of a valuable education</h1>
                        <p>A more inspired future is promised by immersing ourselves in STEM-oriented education.”</p>
                        <a class="bluebrdlink mt-3" href="about.php">Read More</a>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ slider -->

    <!-- about intro -->
    <section class="about-intro">       
        <!-- container -->
        <div class="container">
            <div class="articleSection">
                <h3 class="animate__animated animate__fadeInUp wow">WELCOME TO iSTEMi</h3>
                <h2 class="animate__animated animate__fadeInUp wow">Give Your Kid the Best Possible Start </h2>
                <p class="animate__animated animate__fadeInUp wow">The International STEM Initiative paves a path to discovery and application of valuable knowledge of the essence of our universe. Taking big STEM concepts to the next level by applying it on an even bigger scale through group work and communication, not only do students get an extensive experience, they learn crucial life skills.” </p>               
                <a href="programs.php" class="bluebrdlink animate__animated animate__fadeInUp wow">View all Programs <span
                        class="icon-long-arrow-right icomoon"></span> </a>               
            </div>
        </div>
        <!--/ container -->
    </section>
    <!--/ about intro -->

    <!-- services -->
    <section class="services-home">
       <!-- conrtainer fluid -->
       <div class="container-fluid px-0">
           <!-- row -->
           <div class="row no-gutters">
               <!-- col -->
               <?php 
               for($i=0;$i<count($serviceCol);$i++) { ?>
               <div class="col-md-6 service-col">
                    <div class="lt-service">
                        <a href="<?php echo $serviceCol[$i][0]?>">
                            <img src="img/<?php echo $serviceCol[$i][1]?>.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="rt-service">
                        <a class="animate__animated animate__fadeInUp wow" href="<?php echo $serviceCol[$i][0]?>"><span class="<?php echo $serviceCol[$i][2]?> icomoon"></span></a>
                        <h4 class="animate__animated animate__fadeInUp wow">
                            <a href="<?php echo $serviceCol[$i][0]?>"><?php echo $serviceCol[$i][3]?></a>
                        </h4>
                        <p class="animate__animated animate__fadeInUp wow text-center"><?php echo $serviceCol[$i][4]?></p>
                    </div>
               </div>
               <?php } ?>
               <!--/ col -->
           </div>
           <!--/ row -->
       </div>
       <!--/ container fluid -->
    </section>
    <!--/ services -->

    <!-- our founders -->
    <div class="founderSection">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <article class="articleTitle text-center">
                        <h3 class="h3 fbold text-uppercase">Our faculties</h3>
                    </article>
                </div>
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row pt-3 justify-content-center">
                <!-- col -->
                <div class="col-md-3 animate__animated animate__fadeInUp wow">
                    <div class="picCol">
                        <img src="img/tanviRavilla.jpg" alt="" class="img-fluid">
                        <h5>Tanvi Ravilla</h5>
                        <p>Edison High STEM Academy</p>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                <div class="col-md-3 animate__animated animate__fadeInDown wow">
                    <div class="picCol">
                        <img src="img/soumyaJoshi.jpg" alt="" class="img-fluid">
                        <h5>Soumya Joshi</h5>
                        <p>Edison High STEM Academy</p>
                    </div>
                </div>
                <!--/ col -->    
                 <div class="col-md-6 align-self-center">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/U2gNfbkVfbA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>           
                </div>
                <!--/ row -->

        </div>
        <!--/ container -->
    </div>
    <!--/ our founders -->

    <!-- testimonials home page-->
    <div class="testimonialsHome d-none">
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                 <!-- col -->
                 <div class="col-md-8">
                     <article class="articleTitle text-center pb-3">
                        <h3 class="h3 fbold text-uppercase fwhite">Testimonials</h3>
                      </article>

                      <!-- slider -->
                      <div class="testSlider pt-4">
                         <!-- Swiper -->
                        <div class="swiper-container homeTestimonaisl">
                            <div class="swiper-wrapper">
                                <!-- swiper slide -->
                                <div class="swiper-slide">
                                    <div class="swiper-zoom-container d-block">
                                        <div>
                                            <img src="img/founder01.jpg" alt="" class="testThumb">
                                        </div>
                                        <article class="py-2">
                                             <h5 class="h5 fbold pb-0 mb-0">Student Name will be here</h5>
                                             <p><small class="forange">B.Tech Student</small></p>
                                             <p class="pt-3">“Cras dapibus ullamcorper dictum. Vivamus nec erat placerat felis scelerisque porttitor in ac turpis. In nec imperdiet turpis. Suspendisse quis orci ut orci pulvinar eleifend. Nulla eu mattis ipsum. Integer eget sagittis nulla.”</p>
                                        </article>
                                    </div>                                    
                                </div>
                                <!--/ swiper slide -->

                                <!-- swiper slide -->
                                 <div class="swiper-slide">
                                    <div class="swiper-zoom-container d-block">
                                        <div>
                                            <img src="img/founder01.jpg" alt="" class="testThumb">
                                        </div>
                                        <article class="py-2">
                                             <h5 class="h5 fbold pb-0 mb-0">Student Name will be here</h5>
                                             <p><small class="forange">B.Tech Student</small></p>
                                             <p class="pt-3">“Cras dapibus ullamcorper dictum. Vivamus nec erat placerat felis scelerisque porttitor in ac turpis. In nec imperdiet turpis. Suspendisse quis orci ut orci pulvinar eleifend. Nulla eu mattis ipsum. Integer eget sagittis nulla.”</p>
                                        </article>
                                    </div>
                                </div>
                                <!--/ swiper slide -->

                                <!-- swiper slide -->
                                 <div class="swiper-slide">
                                    <div class="swiper-zoom-container d-block">
                                        <div>
                                            <img src="img/founder01.jpg" alt="" class="testThumb">
                                        </div>
                                        <article class="py-2">
                                             <h5 class="h5 fbold pb-0 mb-0">Student Name will be here</h5>
                                             <p><small class="forange">B.Tech Student</small></p>
                                             <p class="pt-3">“Cras dapibus ullamcorper dictum. Vivamus nec erat placerat felis scelerisque porttitor in ac turpis. In nec imperdiet turpis. Suspendisse quis orci ut orci pulvinar eleifend. Nulla eu mattis ipsum. Integer eget sagittis nulla.”</p>
                                        </article>
                                    </div>
                                </div>
                                <!--/ swiper slide -->
                            </div>                          
                            <!-- Add Navigation -->
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <!--/ swiper -->
                      </div>
                      <!--/ slider -->
                 </div>
                 <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!--/ testimonials home page -->

    <!-- news blogs -->
    <section class="newsBlogs">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                <div class="col-md-6">
                    <article class="articleTitle text-center pb-3">
                        <h3 class="h3 fbold text-uppercase">Blog</h3>
                    </article>
                </div>
                <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row pt-5 justify-content-center">
                    <!-- col -->
                    <?php 
                    for($i=0;$i<2;$i++) {?>
                    <div class="col-lg-4 col-sm-6" data-aos="fade-up">
                        <div class="card blogcard">
                            <a href="<?php echo $blogItem[$i][3]?>.php">
                                <img class="card-img-top img-fluid" src="img/blog/<?php echo $blogItem [$i][0]?>.jpg" alt="">
                            </a>
                            <div class="card-body position-relative">                           
                                <h6 class="h6 animate__animated animate__fadeInUp wow">
                                    <a href="<?php echo $blogItem[$i][3]?>.php"><?php echo $blogItem [$i][1]?></a>
                                </h6>
                                <p class="animate__animated animate__fadeInUp wow">
                                    <small class="fgray">Posted on <spn><?php echo $blogItem [$i][2]?></spn></small>
                                </p>                           
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
    </section>
    <!--/ news blogs -->
    

    <!---/ main -->

   <?php include 'includes/footer.php' ?>

    <?php include 'includes/scripts.php' ?>
    
</body>
</html>