<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
     ?>
</head>

<body>

   <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3">Blog</h2>                    
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                     <!-- col -->
                    <?php 
                    for($i=0;$i<count($blogItem);$i++) {?>
                    <div class="col-lg-4 col-sm-6" data-aos="fade-up">
                        <div class="card blogcard">
                            <a href="<?php echo $blogItem[$i][3]?>.php">
                                <img class="card-img-top img-fluid" src="img/blog/<?php echo $blogItem [$i][0]?>.jpg" alt="">
                            </a>
                            <div class="card-body position-relative">                           
                                <h6 class="h6">
                                    <a href="blogDetail.php"><?php echo $blogItem [$i][1]?></a>
                                </h6>
                                <p>
                                    <small class="fgray">Posted on <spn><?php echo $blogItem [$i][2]?></spn></small>
                                </p>                           
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>  
    <!---/ main -->

   <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
    
</body>
</html>