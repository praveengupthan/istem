<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
     ?>
</head>

<body>

   <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3"> Impact of STEM Experience on Future Endeavors</h2>      
                    <a href="blog.php" class="fblue">Back to Blogs</a>              
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-md-8">
                        <p>As our world continues to advance significantly, science, technology, engineering, and mathematics are becoming attractive skillsets for innovative workers. Seeking creativity, problem-solving, and comprehension, the prospect of having experience in STEM is encouraging and promising. Together, they make an appealing array of opportunities, as one can see in this Venn diagram:</p>
                        <figure>
                        <h3 class="h4 fbold">Entry Level Jobs for STEM Majors</h3>
                            <img src="img/blog/map01.jpg" alt="" class="img-fluid">
                        </figure>
                         <h3 class="h5 fbold">Science:</h3>
                        <p>By understanding the way our universe works from physics to biology, students understand the concept of a continuous progression of study, constantly vying for new knowledge and comprehension of results. In whatever occupation or studies one may pursue, having the scientific method in your pocket will assuredly. Science poses complex issues facing us today, and students have the opportunity to grapple with them and propose multiple perspectives. It is also important to note that in the depths of science, there is ethical consideration to embrace and understand multiple perspectives. Most importantly, there is no denying that humans are inevitably linked to a part of everything in the natural world. Science offers a field to develop an understanding of this world and everything around it, a quality that is absolutely essential in whatever field is pursued.
</p>
                        <h3 class="h5 fbold">Technology:</h3>
                        <p>There is no denying that technology has made its mark on our lives and will continue leaving such symbols of progression. It will forever undoubtedly be a favorable subject of study and studying the intricacies of such complex developments is nothing short of remarkable. Whether one decides to become a developer for tech companies or not, it is impossible to ignore the innovation, creativity, and focus that is required to study this field.
                        </p>

                        <h3 class="h5 fbold">Engineering:</h3>
                        <p>Whether we notice it or not, engineering is everywhere. Behind the desk you work on, the chair you sit at, and the computers that you use are engineering schematics and intricate ergonomics. Engineering itself is an endless possibility of creation, problem-solving techniques, and out-of-the-box thinking with an endless amount of topics like green engineering, electrical engineering, and biomedical engineering. In any future endeavors, engineering is bound to be found and bound to be an appreciated, attractive subject to understand. </p>

                        <h3 class="h5 fbold">Math:</h3>
                        <p>Mathematical knowledge is crucial in many STEM fields. For example, math is needed to understand medical tests as a doctor or understanding risk using probability. It is one of the main skills in most occupations. Understanding how to analyze and solve problems, approach them methodically, and pay attention to detail while thinking abstractly is an ability many employers values. </p>

                         <h3 class="h5 fbold">Public Speaking/English:</h3>
                        <p>Although many times overlooked, English is crucial in collecting thoughts at a deeper scale and comprehending people around you to formulate intelligent ideas. In addition, practice in public speaking is phenomenal because in almost every occupation, the contribution of creative ideas is valued, and the best way to go about presenting your ideas is understanding how to effectively convey these concepts. </p>

                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>  
    <!---/ main -->

   <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
    
</body>
</html>