<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Summer Program 2021</title>    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
     ?>
</head>
<body>
   <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3">Summer Program 2021</h2>                    
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
        <div class="container">
            <img src="img/summerbanner.jpg" alt="" class="img-fluid w-100">
           <p class="pt-3">Our summer STEM program for middle school was extremely successful and insightful as to which kinds of projects students prized more. All of our students were very proud and passionate about their work throughout the five weeks. As a final recap, let us go over the projects and outcomes of all the tasks. Week One: Our first, introductory challenge was for the students to make a roller coaster! The teams worked hard while using their knowledge of potential, kinetic energy, and gravitational pull. Although the first design of the roller coaster was a big challenge, the teams pulled it off and still made constructions that could withstand smaller examples of natural disasters. We learned more about the jobs of structural engineers and the use of stable materials. Moving forward, week Two! This week, students were encouraged to research and create solutions to climate change. They created their own small worlds as well as rules and regulations that their society would follow to ensure the prevention of climate change. We were extremely impressed as our students found new ways to consume, produce, and re-use energy. The very next week, we did another engineering challenge! Zip Lines! Students were instructed to create a zipline carrier that would safely carry a marble down a zipline that the students helped us set up. By calculating the right slope of the zip-line, and using materials such as metal washers, tape, cardboard, and paper-clips, students were able to create a functioning carrier. </p>
           
           <p>We compared these carriers to today and how this useful movement of goods could be well used to make efficient and safe transportation during Covid-19. Week Four! We researched species extinction and saw the effects of extinction in different ecosystems. Students selected their own specific animal and investigated how the world would be affected with the extinction of their animal. They learned about niche partitioning, producers and consumers in an ecosystem, and invasive species. They also realized that the next mass extinction will most likely be the result of human activities, due to deforestation and habitat destruction. Our final week! Week Five was another engineering activity. Students were given the materials to create a catapult. Students adjusted their steps and started with a rough design first, following the stages of Design Thinking. They worked on creating a catapult holding a marble and tested distance and accuracy. Students used potential and kinetic energy, tension, and mathematical calculations to create a properly functioning mini catapult!               
           </p>

           <div class="row">
               <div class="col-md-3">
                   <img src="img/summerprogram/summerimg01.jpg" alt="" class="img-fluid w-100 mb-2">
               </div>
               <div class="col-md-3">
                   <img src="img/summerprogram/summerimg02.jpg" alt="" class="img-fluid w-100 mb-2">
               </div>
               <div class="col-md-3">
                   <img src="img/summerprogram/summerimg03.jpg" alt="" class="img-fluid w-100 mb-2">
               </div>
               <div class="col-md-3">
                   <img src="img/summerprogram/summerimg04.jpg" alt="" class="img-fluid w-100 mb-2">
               </div>
               <div class="col-md-3">
                   <img src="img/summerprogram/summerimg05.jpg" alt="" class="img-fluid w-100 mb-2">
               </div>
           </div>
        </div>
        <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>
    
    

    <!---/ main -->

   <?php include 'includes/footer.php' ?>

    <script>
         $(document).ready(function(){
      //Vertical Tab
      $('#spectab').easyResponsiveTabs({
          type: 'vertical', //Types: default, vertical, accordion
          width: 'auto', //auto or any width like 600px
          fit: true, // 100% fit in a container
          closed: 'accordion', // Start closed if in accordion view
          tabidentify: 'hor_1', // The tab groups identifier
          activate: function(event) { // Callback function if tab is switched
              var $tab = $(this);
              var $info = $('#nested-tabInfo2');
              var $name = $('span', $info);
              $name.text($tab.text());
              $info.show();
          }
      });
    });
    </script>

    <?php include 'includes/scripts.php' ?>
    
</body>
</html>