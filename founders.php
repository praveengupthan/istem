<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>

    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
    ?>
</head>
<body>

   <?php include 'includes/header.php' ?>

    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3">Founders</h2>                    
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
        <div class="container">
            <!-- row -->
            <div class="row py-3 border-bottom">
                <div class="col-md-3">
                    <img src="img/tanviRavilla.jpg" alt="" class="img-fluid">
                </div>
                 <div class="col-md-9">
                     <h4 class="h4 fsbold text-uppercase pb-2 mb-0 foundertitle">Tanvi Ravilla <small class="fbold fblue small">Co-Founder</small></h4>
                     
                    <p>Tanvi is a Student at Edison High School in New Jersey and is in a specialized program called STEM Academy. She also tampers with the arts, which include Kuchipudi (a classical dance form), acrylic painting, and Telugu classical singing. She also is an archer and an active member of a volunteer special ed. program. A large part of her life is devoted to debate and public speaking. Last year, she spoke in a TedX young people event. She spoke about her experiences with volunteering and the difficulties found in places in poverty. She talked about disabilities commonly found in minority communities. For which, she had to reach out and speak with external non-profit organizations to identify a solution. This was her first large push toward STEM and biology, and made her realize her love for those subjects. Her experience with STEM made it easier for her to speak about topics she was passionate about. This encouraged her to continue public speaking and become more comfortable with her voice. After going to STEM Academy, her yearning to comprehend these fields increased greatly. With this organization, she is extremely excited to share her knowledge and learn so many new things on the way. </p>
                </div>                
            </div>
            <!--/ row -->

             <!-- row -->
            <div class="row py-3">
                <div class="col-md-3 order-md-last ">
                    <img src="img/soumyaJoshi.jpg" alt="" class="img-fluid">
                </div>
                 <div class="col-md-9">
                     <h4 class="h4 fsbold text-uppercase pb-2 mb-0 foundertitle">Soumya Joshi <small class="fbold fblue small">Co-Founder</small></h4>
                     
                    <p>Soumya is a Student in Edison High School in New Jersey and is a part of the STEM Academy. She is a Kathak dancer, Hindi classical singer, and a tennis player alongside her education. She has participated in science fairs, presenting two diverse projects: “Targeted Drug Delivery through Superabsorbent Polymer” and “Temperature's Effect on Catalase Enzyme's Reaction to Hydrogen Peroxide in the Liver” in biomedical sciences. From developing a hypothesis to test with an intricate experiment to assembling materials and methods, she was able to immerse herself in the research process, hands-on activities, and the creative thinking procedures. These experiences along with AP-leveled STEM classes gave her the educated background to excel in the current academic environment of the STEM program, a rigorous curriculum that encompasses college-leveled courses with a selective cohort of students. Along with her co-founder, Soumya looks forward to sharing some of her knowledge with students around the world in order to educate the future generation who can excel in whatever they choose to do in the future.</p>
                </div>                
            </div>
            <!--/ row -->


        </div>
        <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>
    
    

    <!---/ main -->

   <?php include 'includes/footer.php' ?>

    <?php include 'includes/scripts.php' ?>
    
</body>
</html>