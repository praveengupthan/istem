<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
     ?>
</head>

<body>

   <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3"> STEM in Real Life</h2>      
                    <a href="blog.php" class="fblue">Back to Blogs</a>              
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-md-8">
                        <p>Science, technology, engineering, and mathematics are used every single day. From pencils and pens to computers and telephones, our world revolves around STEM. Research shows that learning about STEM increases critical thinking and creativity. It also sparks the curiosity to identify how certain things are made, or how they came to be. </p>

                        <h3 class="h5 fbold">Science:</h3>
                        <p>By learning about science, students enhance their general knowledge of medicine and how organisms and the world function. Science frames biology and each and every part of life. For example, science is involved in eating, cooking, breathing, driving, playing, and more. Furthermore, engineering, technology, and math also play a part in science, like building technology that makes it easier to explore hidden parts of the medical world. STEM allows people to look at a problem from multiple angles and find the solution. It opens up a whole new world of possibilities for everyday life. </p>

                        <h3 class="h5 fbold">Technology:</h3>
                        <p>Technology surrounds the world, from our telephones to wifi, to satellites, to washing machines, to refrigerators. We depend on technology twenty-four/seven. Especially during modern times, life becomes unstable if we can’t communicate or connect with someone by a quick swipe on the phone. By learning about technology and how to enhance technology in our lives, we learn how to make everyday life and activities easier. We learn to modernize and transform the world, by looking at an object or idea and identifying how to make it better. </p>

                        <div class="row">
                            <div class="col-md-4">
                                <img src="img/blog/map02.jpg" alt="" class="img-fluid"> 
                            </div>
                            <div class="col-md-8">
                                <h3 class="h5 fbold">Engineering:</h3>
                                <p> Engineering is a lot like technology, except it’s so much more important to human life and activity. Engineering exists from the beds we wake up in, to airplanes that travel far greater distances than anyone in the 1890s. Learning how to engineer helps improve everyday objects and activities. By applying engineering to the real world, people see parts of life from a whole new aspect. Understanding the effort and process of simple things, like glass, or houses become so much easier and it creates a new look on life. </p>
                            </div>
                        </div>
                        
                         <h3 class="h5 fbold">Mathematics:</h3>
                        <p>There’s a reason students start to learn mathematics by the age of six months. It is because math is a large part of life. We learn to count our fingers and then move onto coins, and then imaginary numbers, but it’s not just because of college algebra. It is because math stimulates general knowledge. It creates a reason for people to understand how many items should go in one basket or the actual length of a bed. It is important because math will help make life easier. Rather than having to measure the whole side of a rectangular bed, knowing the length, height, and volume of the bed, you can easily find the length. </p>
                        <p>One recurring idea with STEM is that it is taught to sharpen problem-solving skills and make life easier and better. </p>                       

                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>  
    <!---/ main -->

   <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
    
</body>
</html>