<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
     ?>
</head>
<body>
   <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3">Programs</h2>                    
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
        <div class="container">
            <!-- tab -->
             <!-- spec tab -->       
            <div class="spectab">
                <!--Vertical Tab-->
                <div id="spectab" class="customTab">
                    <ul class="resp-tabs-list hor_1">
                        <li>Science</li>
                        <li>Technology</li>
                        <li>Engineering</li>
                        <li>Math</li>
                        <li>Public Speaking/English</li> 
                    </ul>

                    <div class="resp-tabs-container hor_1">
                        <!-- tab-->
                        <div>                                   
                            <div class="row pt-3 specrow">
                                <div class="col-md-12 text-center">
                                    <img src="img/laboratory-animate.svg" alt="" class="programImg">
                                </div>

                                <div class="col-md-12 text-center">
                                    <h4 class="fbold fblue h4 text-uppercase">Science</h4>
                                </div>

                                <div class="col-md-12">
                                    <p>From biology to chemistry to physics, this initiative focuses on providing a unique curriculum that encompasses all sects of sciences through labs, experiments, games, and activities. Concepts like gravity, energy, organisms give students a better understanding of human progress, the evolution of our universe, and the ideas of scientific research and experimentation. </p>
                                </div>

                            </div>
                        </div>
                        <!--/ tab-->

                        <!-- tab-->
                        <div>                                   
                            <div class="row pt-3 specrow">
                                <div class="col-md-12 text-center">
                                    <img src="img/robotics-animate.svg" alt="" class="programImg">
                                </div>

                                <div class="col-md-12 text-center">
                                    <h4 class="fbold fblue h4 text-uppercase">Technology</h4>
                                </div>

                                <div class="col-md-12">
                                    <p>This program requires students to use the technology and tools around them to work on projects which encourage creativity and critical thinking skills. Working with familiar objects will help students identify how to make their ideas a reality, building outside their comfort zones and expanding knowledge through various sites and platforms. In this curriculum, students will have the opportunity to utilize technology to their fullest potential, by designing biomes and by researching to solve real world problems. </p>
                                </div>
                            </div>
                        </div>
                        <!--/ tab-->

                         <!-- tab-->
                        <div>                                   
                            <div class="row pt-3 specrow">
                                <div class="col-md-12 text-center">
                                    <img src="img/construction-animate.svg" alt="" class="programImg">
                                </div>

                                <div class="col-md-12 text-center">
                                    <h4 class="fbold fblue h4 text-uppercase pt-4">Engineering</h4>
                                </div>

                                <div class="col-md-12">
                                    <p>A skill that is playing an avid role in our world as we progress, engineering designs are behind every single thing around us.. Students will be exposed to the numerous sects of engineering including green engineering, the engineering design process, schematics, and much more.  As the demand for bright, young minds that are knowledgeable in engineering will increase, we look to nurture and ignite a passion for the intricacies of this subject. </p>
                                </div>
                            </div>
                        </div>
                        <!--/ tab-->

                         <!-- tab-->
                        <div>                                   
                            <div class="row pt-3 specrow">

                                <div class="col-md-12 text-center">
                                    <img src="img/calculator-animate.svg" alt="" class="programImg">
                                </div>

                                <div class="col-md-12 text-center">
                                    <h4 class="fbold fblue h4 text-uppercase">Math</h4>
                                </div>

                                <div class="col-md-12">
                                    <p>The iSTEMi seeks to guide students through applying mathematical techniques along with other concepts in real-world situations. These activities will be thought-provoking as students must incorporate their collective knowledge with new mathematical methods in order to complete and understand the activity. </p>
                                </div>

                            </div>
                        </div>
                        <!--/ tab-->

                         <!-- tab-->
                        <div>                                   
                            <div class="row pt-3 specrow">
                                <div class="col-md-12 text-center">
                                    <img src="img/conference-speaker-animate.svg" alt="" class="programImg">
                                </div>

                                <div class="col-md-12 text-center">
                                    <h4 class="fbold fblue h4 text-uppercase">English</h4>
                                </div>

                                <div class="col-md-12">
                                    <p>Public speaking and displaying constructive thoughts through words is a crucial skill for many future leaders and the iSTEMi seeks to bolster this skill by offering debates and presentational tasks as well as guidance for essays and writing prompts. </p>
                                </div>
                            </div>
                        </div>
                        <!--/ tab-->
                        
                    </div>
                </div>
                <!--/ vertical tab -->
            </div>
            <!--/ spec tab -->
            <!--/ tab -->
        </div>
        <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>
    
    

    <!---/ main -->

   <?php include 'includes/footer.php' ?>

    <script>
         $(document).ready(function(){
      //Vertical Tab
      $('#spectab').easyResponsiveTabs({
          type: 'vertical', //Types: default, vertical, accordion
          width: 'auto', //auto or any width like 600px
          fit: true, // 100% fit in a container
          closed: 'accordion', // Start closed if in accordion view
          tabidentify: 'hor_1', // The tab groups identifier
          activate: function(event) { // Callback function if tab is switched
              var $tab = $(this);
              var $info = $('#nested-tabInfo2');
              var $name = $('span', $info);
              $name.text($tab.text());
              $info.show();
          }
      });
    });
    </script>

    <?php include 'includes/scripts.php' ?>
    
</body>
</html>