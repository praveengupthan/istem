


   
   
   //header add class
  $(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $("header").addClass("fixed-top");
    } else {
        $("header").removeClass("fixed-top");
    }
  });
     
      
    //on click move to browser top
    $(document).ready(function(){
        $(window).scroll(function(){
            if($(this).scrollTop() > 50){
                $('#movetop').fadeIn()
            }else{
                $('#movetop').fadeOut();
            }
        });
    
        //click event to scroll to top
        $('#movetop').click(function(){
            $('html, body').animate({scrollTop:0}, 400);
        });
    });
 

    var swiper = new Swiper('.home-swiper', {
      spaceBetween: 0,
      centeredSlides: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });


    $(document).ready(function(){
      //Vertical Tab
      $('#spectab').easyResponsiveTabs({
          type: 'vertical', //Types: default, vertical, accordion
          width: 'auto', //auto or any width like 600px
          fit: true, // 100% fit in a container
          closed: 'accordion', // Start closed if in accordion view
          tabidentify: 'hor_1', // The tab groups identifier
          activate: function(event) { // Callback function if tab is switched
              var $tab = $(this);
              var $info = $('#nested-tabInfo2');
              var $name = $('span', $info);
              $name.text($tab.text());
              $info.show();
          }
      });
    });



    //home page testimonials 
     var swiper = new Swiper('.homeTestimonaisl', {
      zoom: true,
      pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });


   

