<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
     ?>
</head>

<body>

   <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3">Curriculum</h2>                    
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
            <div class="container">
                <p><b>Course description:</b> iSTEMi seeks to target these programs through a combination of highly thought-provoking and diverse activities that ensures a more well-rounded, knowledgeable student.
</p>

                <div class="table-responsive">
                    <table class="table table table-striped curriTable">                   
                        <tbody>     
                            <tr>
                                <td>Week 1</td>   
                                <td>Solutions to Climate Change/Global Warming</td>                        
                                <td>Students must come up with innovative and reasonable ways of living to reduce climate change by studying the excessive carbon dioxide emissions and burning of fossil fuels.</td>
                            </tr>

                            <tr>
                                <td>Week 2</td>   
                                <td>NASA Mathematics Activity</td>                        
                                <td>An advanced activity that includes the science of space exploration, mathematics, a bit of engineering/hands-on experience </td>
                            </tr>

                            <tr>
                                <td>Week 3</td>   
                                <td>Paper Plate Marble Roller Coaster </td>                        
                                <td>Create a roller coaster with simple materials to learn about ideas of physics like gravitational potential energy and kinetic energy. </td>
                            </tr>

                            <tr>
                                <td>Week 4</td>   
                                <td>Biome Exploration</td>                        
                                <td>A competition that challenges students' researching ability, collaboration skills, and creativity all while studying the biomes of the world </td>
                            </tr>

                            <tr>
                                <td>Week 5-6</td>   
                                <td>Public Speaking and Exposure to English </td>                        
                                <td>Students will be introduced to a variety of methods that are key to synthesizing an impressive essay. This includes ways of writing introductions, literary analysis, short stories, and more.  Using their previous knowledge from the previous lesson on ways to construct their thoughts, students will be introduced to key ideas of public speaking and debate in order to present their stance on certain topics. </td>
                            </tr>

                            <tr>
                                <td>Week 7</td>   
                                <td>Engineering a Filter for Water </td>                        
                                <td>Students may come up with a design for a portable, compact filtration system. This establishes one of the most important parts of the engineering process: the design. </td>
                            </tr>

                            <tr>
                                <td>Week 8</td>   
                                <td>Zipline</td>                        
                                <td>Students must construct a zipline to replicate a feasible transport method used in stores for safe exchange of goods without virus transmission. This is an excellent way to study friction, air resistance, motion, mass, and gravity </td>
                            </tr>

                            <tr>
                                <td>Week 9</td>   
                                <td>Hovercraft</td>                        
                                <td>Students are asked to create a tabletop hovercraft with household items and a CD to investigate their background knowledge on aerodynamics. </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>  
    <!---/ main -->

   <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
    
</body>
</html>