<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
     ?>
</head>

<body>

   <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3">Contact</h2>                    
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

            <!-- sub page content -->
            <section class="subpageContent">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-8 col-sm-12"> 
                        <?php
                            if(isset($_POST['submitContact'])){
                            $to = "istemiusa@gmail.com"; 
                            $subject = "Mail From ".$_POST['name'];
                            $message = "
                            <html>
                            <head>
                            <title>HTML email</title>
                            </head>
                            <body>
                            <p>".$_POST['name']." has sent mail!</p>
                            <table>
                            <tr>
                            <th align='left'>Name</th>
                            <td>".$_POST['name']."</td>
                            </tr>
                            <tr>
                            <th align='left'>Contact Number</th>
                            <td>".$_POST['phone']."</td>
                            </tr>
                            <tr>
                            <th align='left'>Email</th>
                            <td>".$_POST['email']."</td>
                            </tr>
                            <tr>
                            <th align='left'>How did you first hear of us</th>
                            <td>".$_POST['howDid']."</td>
                            </tr>
                            <tr>
                            <th align='left'>Subject</th>
                            <td>".$_POST['sub']."</td>
                            </tr>
                            <tr>
                            <th align='left'>Message</th>
                            <td>".$_POST['msg']."</td>
                            </tr>
                            </table>
                            </body>
                            </html>
                            ";

                            // Always set content-type when sending HTML email
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                            // More headers
                            $headers .= 'From:' .$_POST['name']. "\r\n";
                            //$headers .= 'Cc: myboss@example.com' . "\r\n";

                            mail($to,$subject,$message,$headers);   

                            //success mesage
                            ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Mail Sent Successfully. Thank you <?= $_POST['name'] ?>, we will contact you shortly.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <?php

                            // echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
                            }
                            ?>
                            <h2 class="h2 fsbold">Let’s build something great together</h2>                            

                            <!-- form -->
                            <form id="contact_form" class="form pt-2" action="" method="post">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-group customForm">
                                            <label>Name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="name" >
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-group customForm">
                                            <label>Phone Number (Optional)</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="phone" >
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-group customForm">
                                            <label>Email Address</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="email" >
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->                           
                                    
                                  
                                    <!-- col -->
                                    <div class="col-md-6">
                                        <div class="form-group customForm">
                                            <label>Subject</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="sub" >
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-12">
                                        <div class="form-group customForm">
                                            <label>Message</label>
                                            <div class="input-group">
                                                <textarea class="form-control" name="msg" style="height:100px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-md-12">                           
                                        <button class="btn bluebrdlink w-100" name="submitContact">Submit</button>                            
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->
                            </form>
                            <!--/ form -->
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 rtContact col-sm-12">
                            <div class="address">
                                <table class="table table-borderless">
                                    <tr>
                                        <td class="fsbold">Address</td>
                                        <td>:</td>
                                        <td>
                                            <h6><b>iSTEMi</b></h6>
                                            <p class="text-left">902 Oak Tree Road, South Plainfield</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fsbold">Contact</td>
                                        <td>:</td>
                                        <td>
                                            <p class="pb-0">+1 (908) 248-2775</p>
                                            <!-- <p>+91 7995165789</p> -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fsbold">Email</td>
                                        <td>:</td>
                                        <td>
                                            <p class="pb-0"> istemiusa@gmail.com</p>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <!--/ col -->
                    </div>
                    <!--/row -->   
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page content -->
         
    </main>  
    <!---/ main -->
   <?php include 'includes/footer.php' ?>
   <?php include 'includes/scripts.php' ?>
    
</body>
</html>