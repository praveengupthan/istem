<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iSTEMi</title>

    
     <?php 
        include 'includes/arrayObjects.php';
        include 'includes/styles.php';
    ?>
</head>
<body>

   <?php include 'includes/header.php' ?>

    <!-- main -->
    <main class="subpageMain">
        <!-- about header -->
        <section class="about-header">
            <!-- container -->
            <div class="container">
               <div class="articleSection">
                    <h2 class="fblue pb-3">About iSTEMi</h2>                    
               </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ about heder -->

        <!-- sub page content -->
        <section class="subpageContent">
             <!-- contaainer -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-6">
                    <img src="img/aboutImg01.jpg" alt="" class="img-fluid">
                </div>
                <div class="col-md-6 align-self-center">
                    <h2 class="h3 fbold">About Organization</h2>
                    <p>iSTEMi is an organization with passionate founders who look to present an opportunity for the students to explore their creativity and passions, ignite friendly competitions, and potentially find new perspectives of the world around them. Our ultimate goal is to spread and share our unique knowledge with those that may not have been exposed to our views of study. Displaying students with more chances to apply their knowledge of STEM into the world by exposing them to universal topics, iSTEMi is dedicated to guiding the young, future leaders down a path of success.  </p>
                </div>               
            </div>
            <!--/ row -->

             <!-- row -->
            <div class="row py-3">
                <div class="col-md-6 order-md-last">
                    <img src="img/aboutImg02.jpg" alt="" class="img-fluid">
                </div>
                <div class="col-md-6 align-self-center">
                    <h2 class="h3 fbold">Vision and Mission</h2>
                    <p>Not only will we indulge in the depths of Science, Technology, Engineering, and Mathematics, but we also hope to provide writing, public speaking, and debate practices to encourage and nurture speaking skills, a crucial trait in expressing constructive thoughts in future endeavors. Together, as students and friends, this initiative leads a body of inspired students down a path where passions are nurtured, explorations are encouraged, and goals are one step closer.  </p>
                </div>               
            </div>
            <!--/ row -->


        </div>
        <!--/ container -->
        </section>
        <!--/ sub page content -->

         
    </main>
    
    

    <!---/ main -->

   <?php include 'includes/footer.php' ?>

    <?php include 'includes/scripts.php' ?>
    
</body>
</html>